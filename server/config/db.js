'use strict'

const Sequelize = require('sequelize');
const env = require('./env');

const sequelize = new Sequelize(env.DATABASE_NAME, env.DATABASE_USERNAME, env.DATABASE_PASSWORD, {
  host: env.DATABASE_HOST,
  port: env.DATABASE_PORT,
  dialect: env.DATABASE_DIALECT,
  define: {
    underscored: true
  }
});

// Connect all the models/tables in the database to a db object,
//so everything is accessible via one object
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//Models/tables
db.category = require('../models/category.js')(sequelize, Sequelize);
db.city = require('../models/city.js')(sequelize, Sequelize);
db.country = require('../models/country.js')(sequelize, Sequelize);
db.post = require('../models/post.js')(sequelize, Sequelize);
db.post_location = require('../models/post_location.js')(sequelize, Sequelize);
db.users = require('../models/users.js')(sequelize, Sequelize);

//Relations
db.posts.belongsTo(db.category);
db.posts.belongsTo(db.users);
db.posts.belongsTo(db.post_location);
db.post_location.hasMany(db.posts);
db.users.belongsTo(db.city);
db.users.belongsTo(db.users);
db.category.hasMany(db.posts);
db.city.belongsTo(db.country);
db.city.hasMany(db.users);
db.country.hasMany(db.city);



module.exports = db;