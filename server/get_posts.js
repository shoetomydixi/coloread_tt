'use strict';

const Promise = require('bluebird');

module.exports = (db) => {

  db.posts.findAll({
      include: [{
          model: db.user,
          include: [{
              model: db.user
          },
          {
            model: db.city,
            include: [{
                  model: db.country
            }]
          }]
      },
      {
          model: db.post_location
      }]
  }).then(posts => {
      const resObj = posts.map(post => {
          return Object.assign({}, {
              objectId: post.objectId,
              text: post.text,
              location: post.location.map(location => {
                  return Object.assign({}, {
                      point: location.point
                  })
              }),
              author: post.users.map(user => {
                  return Object.assign({}, {
                      objectId: user.objectId,
                      employer: user.user.map(employer => {
                          return Object.assign({}, {
                              objectId: employer.objectId,
                              createdAt: employer.createdAt
                          })
                      },
                      city: user.city.map(city => {
                          return Object.assign({}, {
                              objectId: city.objectId,
                              name: city.name,
                              country: user.country.map(country => {
                                  return Object.assign({}, {
                                      objectId: country.objectId,
                                      name: country.name,
                                      createdAt: country.createdAt
                                  })
                              })
                          })
                      })
                  })
              })
          })
      });
      return new Promise(function(resolve, reject) {
        return resolve(JSON.stringify(resObj));
      });      
  });

};