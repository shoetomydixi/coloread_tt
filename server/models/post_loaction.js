'use strict'

module.exports = (sequelize, DataTypes) => {
  const Post_Loaction = sequelize.define('post_loaction', {
    objectId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    point: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    underscored: true
  });
  return Post_Loaction;
};