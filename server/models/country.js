'use strict'

module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define('country', {
    objectId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    underscored: true
  });
  return Country;
};