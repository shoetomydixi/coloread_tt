'use strict'

module.exports = (sequelize, DataTypes) => {
  const City = sequelize.define('city', {
    objectId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    country: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    underscored: true
  });
  return City;
};